# backbone-validation-backend

This package lets you show validation errors from the back end in the front end on the corresponding input. It uses [Backbone.Validation](https://github.com/thedersen/backbone.validation).

The back end needs to follow our validation error standard and return 400. An example JSON response:

```json
{
    "error": {
        "code": "ParameterError",
        "validation_errors": {
            "username": [
                { "code": "unique" }
            ],
            "password": [
                { "code": "min", "min": "8" },
                { "code": "password" }
            ]
        }
    }
}
```

## Usage
On app initialization you can define some options, like this:

```js
var validation = require('backbone-validation-backend');

validation({
    classErrorWrapper: '._plz-wrap-my-error',
    messages: {
        mimetype: 'You should use the {1} mimetype.',
    },
    translations: {
        minLength: 'min',
    },
});
```

When adding an error message to an input, it first tries to search for `options.classErrorWrapper` (by default `._error-wrapper`) to append the error text to. If the class can not be found, the direct parent of the input will be used.

Mapping errors from the back end to the front end works on every XHR request (it bypasses Backbone.Validations requirement for a model). An example usage:

```js
var validationBackend = require('backbone-validation-backend/convert-backend');

return Marionette.ItemView.extend({
    submit: function (e) {
        var xhr = this.model.save(null, {
            success: function () {
                validationBackend(xhr, this, options);
            }.bind(this),
            error: function () {
                validationBackend(xhr, this, options);
            }.bind(this),
        });
    }
});
```

Or:

```js
var xhr = $.ajax({
    type: 'POST',
    url: 'api/user_username_check',
});

xhr.always(function () {
    validationBackend(xhr, this, options);
}.bind(this));
```

## Options

You can use `options` for example to define a custom `getEl`:

```
options = {
    getEl: (view, attr) => {
    if (attr === 'name') {
        return this.ui.nameInput.parent();
    } else {
        return Backbone.Validation.callbacks.getEl(this, attr);
    }
};
```

The options will overwrite anything you defined in your `Backbone.Validation.callbacks` setting.

### options.messages
Define custom messages which for a specific backend error. Example:

```
validationBackend(response, view, {
    messages: {
        email: {
            unique: 'This email is already taken. Please choose another.'
        }
    }
});
```

You can define these overrides anywhere you want, but on a model it would seem best:

```
Model.extend({
    validation: {
        ...
    },
    validationBackendMessages: {
        email: {
            unique: 'This email is already taken. Please choose another.'
        }
    }
})
```

Then you can call as follows:

```
validationBackend(response, view, view.model.validationBackendMessages);
```

# Changelog

## 0.3.2
- Support empty xhr.

## 0.3.1
- Remove unnecessary delete and set default options.

## 0.3.0
- Add support for options.messages.

## 0.2.0
- Add support for options.