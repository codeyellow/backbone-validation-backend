define(function (require) {
    'use strict';
    var _ = require('underscore');

    require('backbone-validation');

    var defaultOptions = {
        // Class where the error text will be appended to. If not found, the parent from the input is used.
        classErrorWrapper: '_error-wrapper',
        classHasError: 'has-error',
        classErrorText: 'error-text',
        translations: {},
        messages: {},
        validators: {},
        callbacks: {},
    };

    var messages = {
        required: 'This field is required.',
        acceptance: 'This field must be accepted.',
        min: 'This field must be greater than or equal to {1}.',
        max: 'This field must be less than or equal to {1}.',
        range: 'This field must be between {1} and {2}.',
        length: 'This field must be {1} characters.',
        minLength: 'This field must be at least {1} characters.',
        maxLength: 'This field must be at most {1} characters.',
        rangeLength: 'This field must be between {1} and {2} characters.',
        oneOf: 'This field must be one of: {1}.',
        equalTo: 'This field must be the same as {1}.',
        digits: 'This field must only contain digits.',
        number: 'This field must be a number.',
        email: 'This field must be a valid email.',
        url: 'This field must be a valid url.',
        inlinePattern: 'This field is invalid.',

        // Back-end validation messages.
        unique: 'This value is not unique.',
        exists: 'This value does not exist.',
        password: 'This password is not strong enough.',
        date: 'This value must be a valid date.',
        date_format: 'This value does not match the format {1}.',
        after: 'This value must be a date after {1}.',
        before: 'This value must be a date before {1}.',
        mimes: 'This file must be of type: {1}.',
        image: 'This file must be an image.',
    };

    var translations = {
        size: 'length',
        accepted: 'acceptance',
        same: 'equalTo',
        integer: 'digits',
        numeric: 'number',
        in: 'inlinePattern',
        not_in: 'inlinePattern',
        regex: 'inlinePattern',
        filled: 'required',
        active_url: 'url',
    };

    return function (options) {
        options = _.extend({}, defaultOptions, options);

        _.extend(Backbone.Validation.messages, messages, options.messages);

        // Add custom validators here.
        // Check: http://thedersen.com/projects/backbone-validation/#extending-backbone-validation
        _.extend(Backbone.Validation.validators, options.validators);

        var callbacks = {
            // The element to bind the error to.
            // First, try to find a class to bind it to, if it can not be found,
            // fallback to the parent of the name attribute.
            getEl: function (view, attr) {
                var nameEl = view.$('[name="' + attr + '"]'),
                    closest = nameEl.closest('.' + options.classErrorWrapper, view);
                if (closest.length) {
                    return closest;
                }
                return nameEl.parent();
            },
            // The input is valid, so remove the error that was appended.
            valid: function (view, attr, selector) {
                var $el = this.getEl(view, attr);

                $el.removeClass(options.classHasError);
                $el.find('.' + options.classErrorText).remove();
            },
            // The input is invalid, so add an error message to it.
            invalid: function (view, attr, error, selector) {
                var $el = this.getEl(view, attr),
                    $errorText = $el.find('.' + options.classErrorText);

                $el.addClass(options.classHasError);

                if ($errorText.length) {
                    $errorText.html(error);
                } else {
                    $el.append('<div class="' + options.classErrorText + '">' + error + '</div>');
                }
            },
        };

        _.extend(Backbone.Validation.callbacks, callbacks, options.callbacks);

        Backbone.Validation.backend = {};
        Backbone.Validation.backend.translations = _.extend({}, translations, options.translations);

    };
});
