define(function (require) {
    'use strict';
    /**
     * Handle back-end validation errors by displaying them like errors from Backbone.Validation.
     */

    // Replaces numeric placeholders like {0} in a string with arguments
    // passed to the function.
    // Copied from Backbone-Validation.
    var formatPlaceholders = function () {
        var args = Array.prototype.slice.call(arguments),
            text = args.shift();
        return text.replace(/\{(\d+)\}/g, function(match, number) {
            return typeof args[number] !== 'undefined' ? args[number] : match;
        });
    };

    // Convert validation messages from back-end to Backbone.Validation's messages.
    var convertBackendCodes = function (code) {
        var translations = Backbone.Validation.backend.translations;

        return translations[code] || code;
    };

    /**
     * Get the full message along with extra values that are passed.
     * E.g. {"code": "size", "size": 2}, returns:
     * ['This field must be {1} characters.', 'phonenumber', 2]
     *
     * @param {Object} messages Overrides for Backbone.Validation.messages. Example: {email: {unique: 'This email adress is already taken'}}
     */
    var objectValuesToArray = function (obj, fieldName, messages) {
        var code = convertBackendCodes(obj.code),
            message = (messages[fieldName] ? messages[fieldName][code] : false) || Backbone.Validation.messages[code] || code,
            arr = [message, fieldName];

        // Push each value except for 'code' (because we have already mapped it),
        // onto an array.
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                arr.push(obj[key]);
            }
        }

        return arr;
    };

    // Map validation errors from the xhr response to a name attribute,
    // and display the correct error.
    var mapErrors = function (xhr, view, options = {}) {
        var callbacks = _.extend({}, Backbone.Validation.callbacks, options);

        if (
            xhr.responseJSON && xhr.responseJSON.error &&
            xhr.responseJSON.error.validation_errors
        ) {
            var validationErrors = xhr.responseJSON.error.validation_errors;

            _.each(validationErrors, function (field, fieldName) {
                var values = objectValuesToArray(field[0], fieldName, options.messages ? options.messages : {}),
                    errorText = formatPlaceholders.apply(this, values);

                callbacks.invalid(view, fieldName, errorText);
            });
        }
    };

    // Clear all errors on inputs in the view that have a name attribute.
    var clearErrors = function (view) {
        var inputNames = Backbone.Validation.attributeLoaders.inputNames(view);

        inputNames.forEach(function (name) {
            Backbone.Validation.callbacks.valid(view, name);
        });
    };

    return function (xhr, view, options) {
        clearErrors(view);

        if (xhr && xhr.status === 400) {
            mapErrors(xhr, view, options);
        }
    };
});
